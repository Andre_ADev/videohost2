# Используем официальный образ Python в качестве базового
FROM python:3.10-slim

# Устанавливаем рабочий каталог в контейнере
WORKDIR /app

# Копируем файл зависимостей в рабочий каталог
COPY requirements.txt /app/

# Устанавливаем зависимости, указанные в requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

# Копируем все файлы проекта в рабочий каталог
COPY . /app/

# Открываем порт 8000 для доступа к приложению
EXPOSE 8000

# Запускаем сервер разработки Django
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
