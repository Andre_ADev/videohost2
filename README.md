Проект дорабатывается.

Запуск проекта с помощью Docker-образа:
1) Клонирование: 
git clone https://gitlab.com/Andre_ADev/videohost2.git
2) Заходим в папку videohost2:
cd videohost2
3) Создаем образ:
docker build -t videohost-app .
4) Запускаем образ:
docker run -p 8000:8000 videohost-app
5) Заходим в браузер и вводим url:
localhost:8000

Для связи со мной - @AndreyAgEEvf (Telegram)